# csgo_st
## Counter Strike: Global Offensive Song Transmitter

# Dependencies

* sox
* sed
* wc
* zenity


# Usage

-d: default behaviour, choose songs and play them

-a: append, Append to the current songs list

-n: next song, Play the next song

-p: Previous song, Play the previous song

-w: Write cfg, Writes the necessary config file in csgo config directory

# Todo
1. Load CSGO_PATH and Bind Toggle Key from a config file if available
2. Complete the Dependencies list 
3. Optionally add lib notify
4. Create a simple man page and add a -h help option to the script
